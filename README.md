# README #

Rabbit WebGL project - display the rabbit.ply in WebGL

### For my internal use, to save important stages and branches ###

Main files are:  
 * rabbit.html, simple and basic HTML to create the canvas
 * rabbit.js, the JS that loads rabbit data and displays in Three
 * rabbit.ply - source data
 * rabbit.json formatted JSON data
 * rabbitData.py Python code to convert source .ply to useable .json