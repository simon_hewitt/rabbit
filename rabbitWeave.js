/**
* Created by simon on 30/05/15.
To create the JSON dataset, run
python3 rabbitData.py -s 150
where -s is the scale, 150 fills the screen nicely
*/



// once everything is loaded, we run our Three.js stuff.
//
function makeRabbit ()
{

    var stats = initStats();

    var __ret = basicSetup();
    var scene = __ret.scene;
    var camera = __ret.camera;
    var renderer = __ret.renderer;
    var plane = __ret.plane;
    var spotLight = __ret.spotLight;
    var spotLight3 = __ret.spotLight3;

    var sphere1 = makeMarkerSpheres(scene, 20);

    loadRabbit();

    // add the output of the renderer to the html element
    $("#WebGL-output").append(renderer.domElement);
    var controls = makeGui();
    render();

    function moveObjectAbsolute (obj, x, y, z) {
        // This moves each vertex point, so the mesh position remains unchanged after the move
        obj.geometry.vertices.map(function (v) {v.x += x; v.y += y; v.z += z});
    }

    function loadRabbit()
    {
        // Load the Rabbit JSON data using a THREE loadTexture

        var loader = new THREE.JSONLoader();
        loader.load( "rabbit.json", rabbitLoaded );  // Calls rabbitLoaded on completion
    }

    function rabbitLoaded( geometry, materials ) {
        // Called when rabbit dataset loaded asynchronously
        console.log("rabbit Loaded !!");

        var texture = THREE.ImageUtils.loadTexture ("assets/textures/general/weave_512x512.jpg")
        texture.magFilter = THREE.LinearMipMapNearestFilter;
        var meshMaterial = new THREE.MeshBasicMaterial(0x4f3caa );
        //var meshMaterial = new THREE.MeshPhongMaterial({color: 0x363cc9});
        meshMaterial.map = texture;

        //texture.minFilter = THREE.LinearFilter;
        //meshMaterial.map = texture;

        // create the rabbit. make global
        var rm = new THREE.Mesh(geometry, meshMaterial);

        rabbitMesh = rm;
        spotLight.target = rabbitMesh;
        spotLight3.target = rabbitMesh;
        scene.add( rabbitMesh );
    }


    function render() {
        stats.update();

        var renderIterations = 0.5;

        // rotate the cubes around its axes
        scene.traverse(function (e) {
            if ((e instanceof THREE.Mesh || e instanceof THREE.PointCloud) && e != plane) {

                e.rotation.x += controls.rotationSpeed;
                e.rotation.y -= controls.rotationSpeed;
                renderIterations = e.rotation.y;
                //e.rotation.z += controls.rotationSpeed;
                var s = controls.pointScale;
                e.scale.x = s;
                e.scale.y = s;
                e.scale.z = s;
            }

            if (typeof rabbitMesh !== 'undefined') {
                // As this is called long before rabbit gets loaded (as its asynchronous)
                rabbitMesh.position.x = controls.spX;
                rabbitMesh.position.y = controls.spY;
                rabbitMesh.position.z = controls.spZ;
            }
        });

        // render using requestAnimationFrame
        requestAnimationFrame(render);
        renderer.render(scene, camera);
    }


    function initStats() {

        var stats = new Stats();

        stats.setMode(0); // 0: fps, 1: ms

        // Align top-left
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';

        $("#Stats-output").append(stats.domElement);

        return stats;
    }


    function groundPlane() {
        var planeGeometry = new THREE.PlaneBufferGeometry(60, 40, 1, 1);
        var texture = THREE.ImageUtils.loadTexture("assets/textures/ground/grasslight-big.jpg")
        var planeMaterial = new THREE.MeshPhongMaterial({color: 0x2194ce});
        planeMaterial.map = texture;

        var plane = new THREE.Mesh(planeGeometry, planeMaterial);
        plane.receiveShadow = true;
        // rotate and position the plane
        plane.rotation.x = -0.5 * Math.PI;
        plane.position.x = 0;
        plane.position.y = 0;
        plane.position.z = 0;
        return plane;
    }


    function basicSetup() {
        // create a scene, that will hold all our elements such as objects, cameras and lights.
        var scene = new THREE.Scene();
        // create a camera, which defines where we're looking at.
        var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
        // create a render and set the size
        var renderer = new THREE.WebGLRenderer({logarithmicDepthBuffer: true });
        renderer.setClearColor(new THREE.Color(0xEEEEEE));
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.shadowMapEnabled = true;

        // create the ground plane
        var plane = groundPlane();
        scene.add (plane)

        // position and point the camera to the center of the scene
        camera.position.x = -30;
        camera.position.y = 40;
        camera.position.z = 30;
        camera.lookAt(scene.position);
        // add subtle ambient lighting
        var ambientLight = new THREE.AmbientLight(0x0c0c0c);
        scene.add(ambientLight);
        // add spotlight for the shadows
        var spotLight = new THREE.SpotLight(0xffffff);
        spotLight.position.set(-40, 60, -10);
        spotLight.castShadow = true;
        spotLight.shadowCameraVisible = false;
        scene.add(spotLight);

        var spotLight = new THREE.SpotLight(0xffffff);
        spotLight.position.set(40, 60, -10);
        spotLight.castShadow = true;
        spotLight.shadowCameraVisible = false;
        scene.add(spotLight);

        var spotLight3 = new THREE.SpotLight(0xffffff);
        spotLight3.position.set(0, 10, -50);
        spotLight3.castShadow = true;
        spotLight3.shadowCameraVisible = false;
        scene.add(spotLight3);

        return {scene: scene, camera: camera, renderer: renderer, plane: plane, spotLight: spotLight, spotLight3: spotLight3};
    }


    function createText(txt, pos) {

        var fred =  THREE.FontUtils.faces;

        var meshMaterial = new THREE.MeshPhongMaterial({specular: 0xffffff, color: 0xff5555, shininess: 100, metal: true});
        var options = {
            size: 10,
            height: 10,
            weight: "normal",
            font: "helvetiker",
            bevelThickness: 1,
            bevelSize: 0.1,
            bevelSegments: 1,
            bevelEnabled: true,
            curveSegments: 12
        };
        // create a multimaterial
        var txt = new THREE.TextGeometry(txt, options);
        var textObj = THREE.SceneUtils.createMultiMaterialObject(txt, [meshMaterial]);
        textObj.position.x = pos.x;
        textObj.position.y = pos.y;
        textObj.position.z = pos.z;
        return textObj;
    }



    function makeMarkerSpheres(scene, range) {
        // Creates a 'marker' sphere in red, to help locate position in 3D. Not used in production.
        var texture = THREE.ImageUtils.loadTexture ("assets/textures/general/weave_512x512.jpg")
        var sphereGeo = new THREE.SphereGeometry(5, 32, 32);
        var sphereMatl = new THREE.MeshBasicMaterial({color: 0x8a6241});
        sphereMatl.map = texture;

        var sphere1 = new THREE.Mesh(sphereGeo, sphereMatl);
        sphere1.position.set(1,10,1);
        scene.add(sphere1);
        return sphere1;
    }

    function makeGui() {
// call the render function
        var controls = new function () {
            this.rotationSpeed = 0.01;
            this.pointScale = 1.0;
            this.spX = 4;
            this.spY = -5;
            this.spZ = -5;
            this.springHeight = 2;
        };
        var gui = new dat.GUI();
        gui.add(controls, 'rotationSpeed', 0, 0.1);
        gui.add(controls, 'pointScale', 0.1, 15);
        gui.add(controls, 'spX', -25, 25);
        gui.add(controls, 'spY', -5, 25);
        gui.add(controls, 'spZ', -25, 25);
        gui.add(controls, 'springHeight', -10, 12);
        return controls;
    }

};
