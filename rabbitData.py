__author__ = 'simon'

import sys, getopt

## rabbitData.py
# reads rabbit.ply, from the GPU coursework.
# and writes rabbit.json as a THREE jsonLoader data file
'''
rabbit.ply contains ASCII numbers only. There are two types:
1) The vertices, 5 floats on a line, first 3 are X, Y, Z (No idea what 4 and 5 are!)
2) the triangle face indexes, 4 ints and the first int is always '3', then A, B, C indices into the vertex array
samples:
-0.0400442 0.15362 -0.00816685 0.734503 0.5
3 21216 21215 20399

OUTPUT is a Three.js Json file format,
https://github.com/mrdoob/three.js/wiki/JSON-Geometry-format-4
header:
{
  "metadata": {
    "version": 4,
    "type": "Geometry",
    "generator": "rabbitData.py"
  },
   "faces": [
      0,
      0,
      0,
      1,
      etc,
    "vertices": [
    0,
      -0.0378297,
      0.12794,
      0.00447467,

Note faces are quartets as in https://github.com/mrdoob/three.js/wiki/JSON-Model-format-3
type bitmask 0 => triangles, followed by 3 triangle indices (into the vertices array)
        // triangle
        // 00 00 00 00 = 0
        // 0, [vertex_index, vertex_index, vertex_index]
        0, 0,1,2,

      can be many others as well.
'''

class MaxMin:
    def __init__(self):
        self.minX = float("inf")
        self.minY = float("inf")
        self.minZ = float("inf")
        self.maxX = float("-inf")
        self.maxY = float("-inf")
        self.maxZ = float("-inf")
        self.elemntCount = 0;
        self.centroidX = 0;
        self.centroidY = 0;
        self.centroidZ = 0;

    def check (self, x, y, z):
        '''
        :param x, y, z values
        checks and sets new max and min values of elements [0..3], the X, Y, Z vertices
        :return: none
        '''

        if x < self.minX:
            self.minX = x
        if y < self.minY:
            self.minY = y
        if z < self.minZ:
            self.minZ = z

        if x > self.maxX:
            self.maxX = x
        if y > self.maxY:
            self.maxY = y
        if z > self.maxZ:
            self.maxZ = z
        self.elemntCount += 1;
        self.centroidX += x;
        self.centroidY += z;
        self.centroidZ += x;

    def centroid (self):
        return self.centroidX / self.elemntCount,\
            self.centroidY / self.elemntCount,\
            self.centroidZ / self.elemntCount

    def diameter (self):
        x, y, z = self.centroid()



    def __str__(self):
        r = "Max-Min values, X => {1:.3f} : {0:.3f}, Y => {3:.3f} : {2:.3f}, Z => {5:.3f} : {4:.3f},".\
            format(self.maxX, self.minX, self.maxY, self.minY, self.maxZ, self.minZ)
        return r


def getRabbitOpts():
    '''
    Reads the command line options, which can be:
        -h --help -> display a help line and exit
        -s --scale -> A float scale value, exits on bad value
        -:returns
    :return: scale, default 1.0
    '''
    scale = 1.0

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hs:", ["help", "scale="])
        # "hs:" means -h or -s:, [] means --help and/or --scale=
    except getopt.GetoptError:
        print  ("\nrabbitData -s:<scale> | --scale:<scale>\n")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print  ("\nrabbitData -s:<scale> | --scale:<scale>\n")
            sys.exit()
        elif opt in ("-s", "--scale"):
            try:
                scale = float(arg)
            except ValueError:
                print("--scale must be a float value, received : " + arg)
                sys.exit(3)
    return scale




#main

print("\n Reading rabbit.ply into rabbit.json\n", \
        "===================================\n\n", \
      "See the .py comments for details\n")


scale = getRabbitOpts()
print("Scale is : " + str(scale))

lineCount = 0
vertices = []
faces = []
maxMin = MaxMin()


with open ("rabbit.ply", "r") as inFile :
    for  line in inFile:
        lineCount += 1
        elements = line.split(" ")
        if line.startswith ("3 "):
            # Its type 2, face data, like 3 21216 21215 20399
            # Write out THREE.js type 0 (triangle) and 3 vertices
            fLine = '0, {:s}, {:s}, {:s}'.format(elements[1], elements[2], elements[3])
            faces.append(fLine)
        else:
            # Its type 1, vertex data
            x = float(elements[0]) * scale
            y = float(elements[1]) * scale
            z = float(elements[2]) * scale
            vline = '{:f}, {:f}, {:f}'.format(x, y, z)
            vertices.append(vline)
            maxMin.check(x, y, z)


def writeLines(oFile, data):
    comma = ''
    for line in data:
        oFile.write('{:s}\n    {:s}'.format(comma, line))
        comma = ','


with open("rabbit.json", "w") as oFile :
    oFile.write('{\n')
    oFile.write('   "metadata" : {\n')
    oFile.write('    "version" : 4,\n')
    oFile.write('    "type" : "Geometry",\n')
    oFile.write('    "generator" : "rabbitData.py"\n')
    oFile.write('   },\n')
    oFile.write('   "faces" : [')
    writeLines(oFile, faces)
    oFile.write('],\n')
    oFile.write('  "vertices": [')
    writeLines(oFile, vertices)
    oFile.write(']\n')
    oFile.write('}\n')



print ("\nDone, read : ")
print ("  Vertices : {:d}".format(len(vertices)))
print ("  Faces    : {:d}".format(len(faces)))
print (maxMin)
x, y, z = maxMin.centroid();
print ("Centroid, X : {0:.3f}, Y : {1:.3f}, Z : {2:.3f}".format(x, y, z))
if lineCount == (len(vertices) + len(faces)):
    print ("All good")
else:
    print (" ERROR , Lines    : {:d}".format(lineCount))
