__author__ = 'simon'

def printDecimal (n, depth=0) :
    if n < 0:
        print ("-", end="")
        printDecimal(-n, depth)
    elif n >= 10:
        printDecimal(int(n/10), depth+1)
        print (int(n % 10), end="")
        if not depth:
            print("")
    else:
        print(int(n), end="")
        if not depth:
            print("")


printDecimal(-123)
printDecimal(1234567)
printDecimal(1)
printDecimal(-1)
printDecimal(0)
